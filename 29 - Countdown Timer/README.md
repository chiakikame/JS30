# 筆記：第二十九日

這次要打造一個倒數計時器。

## 使用 `Date`

若您要和時間打交道，就必須認識 `Date` 類別。

詳細的文件請參考 [這裡](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)。

## 停止計時器

這裡說的是停止由 `setInterval` 以及 `setTimeout` 發起的計時器。假設您想做一支程式，令其每秒都做一些計算，算完後就停工，您可能可以這樣做：

```js
let jobFinished = false;
setInterval(function timedFunction() {
  if (jobFinished) {
    jobFinished = doComputation();
  }
});
```

但這樣一來，計算完成後，雖然 `doComputation` 已經不會被呼叫，您設定的計時函數（`timedFunction`）仍然還在繼續運作。那該如何讓 JS 執行引擎不要再呼叫 `timedFunction` 呢？該如何停止計時器呢？

您可以這樣做：

```js
let timer;
timer = setInterval(function timedFunction() {
  if (doComputation()) {
    clearInterval(timer);
  }
});
```

由 `setTimeout` 生成的計時器，使用 `clearTimeout` 來停止；由 `setInterval` 生成的計時器則使用 `clearInterval` 停止。

## 更改 HTML 文件標題

修改 `document.title` 可以更改 HTML 文件的標題。瀏覽器會把文件的標題顯示在該網頁所在的分頁（tab）上。若該分頁為當前使用者所瀏覽的分頁，那標題也會一併顯示在瀏覽器程式的標題列上面。

## `submit` 事件

在表單的提交按鈕（`<input type='submit' ...`）上面按一下，或者是表單的文字方塊上面按下輸入按鍵，就會提交表單，觸發 `submit` 事件。

附帶一提，使用 `form.submit()` 會提交表單，但可能不會觸發 `submit` 事件。（https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/submit）

## 實作外加

千秋的實作裡面，計時結束的時候，倒數計時的文字會閃喔！這是利用 CSS 的 `animation` 屬性以及 `@keyframes` 規則做出來的。
