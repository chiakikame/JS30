const timeLeftDisplay = document.querySelector('.display__time-left');
const targetTimeDisplay = document.querySelector('.display__end-time');
const defaultButtons = document.querySelectorAll('.timer__button');

let actingTimer;

function timer(seconds) {
  clearInterval(actingTimer);
  timeLeftDisplay.classList.remove('finished-blinking');
  
  const start = Date.now();
  const end = start + seconds * 1000;
  
  displayTimeInfo(end);
  actingTimer = setInterval(() => {
    if (Date.now() - end >= 0) {
      clearInterval(actingTimer);
      timeLeftDisplay.classList.add('finished-blinking');
    }
    displayTimeInfo(end);
  }, 200);
}

function displayTimeInfo(endTimestamp) {
  const secondsLeft = Math.floor( (endTimestamp - Date.now()) / 1000 );
  const adjustedSecondsLeft = secondsLeft > 0 ? secondsLeft : 0;
  const seconds = adjustedSecondsLeft % 60;
  const minutes = Math.floor(adjustedSecondsLeft / 60);
  timeLeftDisplay.textContent = `${minutes}:${toDoubleDigitNumberString(seconds)}`;
  
  const endTime = new Date(endTimestamp);
  targetTimeDisplay.textContent = `Come back at ${endTime.getHours()}:${toDoubleDigitNumberString(endTime.getMinutes())}`;
}

function toDoubleDigitNumberString(num) {
  return `${num < 10 ? '0' : ''}${num}`;
}

defaultButtons.forEach(button => {
  button.addEventListener('click', () => {
    timer(button.dataset.time);
  });
});

document.querySelector('#custom').addEventListener('submit', function submitHandler (e) {
  e.preventDefault();
  timer(parseInt(this.minutes.value) * 60);
});
