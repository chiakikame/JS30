# 筆記：第八日

這篇主題是，利用 HTML canvas 來建立一個繪圖板

## HTML canvas

Canvas 是一個可以自由作圖的地方。在作圖的時候，要先開啟一個 context。（似乎許多圖形系統都是在 context 上面作圖）。開啟完成後任君使用。

參考資料：
* [Canvas API portal](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API)
* [convas tag](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement)
* [2D context](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D)

## JavaScript

### 滑鼠相關事件的使用

重點事件： `mousedown`, `mouseup`, `mouseout`, `mousemove`。這幾個事件的參數都是 [`MouseEvent`](https://developer.mozilla.org/en/docs/Web/API/MouseEvent)

### 小技巧

示範了 destructing assignment：

```js
[a, b] = [1, 2];
```

更多 destructing assignment：https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment

## 色彩指定

HSL: Hue（色調）, saturation（飽和）, lightness（亮度），是另一種指定色彩的方法。Hue: 0 ~ 360, saturation: 0 ~ 100%, lightness: 0 ~ 100%。指定時如下：

```js
element.color = 'hsl(225, 100%, 75%)';
```

hsl 的調色板：http://mothereffinghsl.com/

## 千秋實作之優缺點

千秋附在這裡的實作，有「中鍵 / 右鍵清除畫面」的功能。

提到缺點，就是事實上，視窗大小重新設定的時候畫上去的東西會消失。可能的解法是，把使用者畫過的點都記起來，然後在畫面消失的時候重現。或許等有時間的時候來改進吧XD
