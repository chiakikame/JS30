# 筆記：第九日

介紹 Chrome 的開發工具

## 在元素屬性變更時暫停程式

打開開發工具（預設 `Ctrl + Shitf + i`）。在想要監視的元件上面按下右鍵，選 `Break on ...` -> `Attribute modifications`。

這樣一來 debugger 會在該元素屬性變更時暫停執行，並且替您指出造成該變化的程式碼。

要停止監視，重複上面的步驟即可。

## `console` 工具

### `log`, `warn`, `error`, `info`

```js
// 一般
console.log("hello world");

// string.format
console.log("hello %s !", "John");

// 用 CSS 使輸出更漂亮
console.log("%c hello world!", "color:blue;");
```

`info`, `warn` 和 `error` 的用法同 `log`，只是會多出警告或者錯誤標誌。

### `assert`

`assert` 用來表示你大膽假設某事情為真。若否則發出錯誤訊息：

```js
console.assert(1 === 1, "Oops, something wrong with this machine!");
```

### `clear`

清除 console 內的所有訊息

### `dir`

`console.log` 雖然可以列出某個元素本身的概略資訊，但像是該元素在頁面的何處、大小、有何事件處理程序已經設定上去的這類的資訊則難以去推敲。此時，您可以使用 `console.dir`，就可以取出關於某個元素超級詳細的資訊。

### `group`, `groupCollapsed`, `groupEnd`

如果想要對 console 輸出的訊息作分組的話，可以把同組的訊息包夾在同一個群組中：

```js
console.group('my-group'); // 用 groupCollapsed 的話，預設會是收起而非展開的狀態
console.log("message 1");
// 更多輸出...
console.groupEnd('my-group');
```

### `count`

計數用，計算同一個 key 在 `count` 中被使用的次數

```js
console.count(1); // 1:1
console.count("gg"); // gg:1
console.count(1); // 1:2
console.count(1); // 1:3
...
```

### `time`, `timeEnd`

計時用。計算同一個 key 從 `time` 到 `timeEnd` 之間所耗費的時間。

```js
console.time("my-timer-key");
longJob(jobParameter)
  .then((result) => {
    // do some post-processing
    console.timeEnd("my-timer-key");
  });
```

### `table`

之前影片中介紹過，若陣列中的物件都有同樣的屬性，那 `table` 就可以幫您用表格呈現這個陣列。
