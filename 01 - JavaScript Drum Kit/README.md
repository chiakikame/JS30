# 筆記：第一日

## CSS

### 布局

打開 `index.html` 可以看到 9 個方塊都是置中的。觀察 `style.css` 後可以發現這是利用 `flex` 來實現的：

```css
.keys {
  display: flex;            /* 使用 flex */
  flex: 1;                  /* flex-grow 設定為 1 */
  min-height: 100vh;        /* 設定最小高度為畫面高度的 100% */
  align-items: center;      /* 子物件垂直置中 */
  justify-content: center;  /* 子物件水平置中 */
}
```

關於 flex 可以參考 https://css-tricks.com/snippets/css/a-guide-to-flexbox/

另外，可以注意到 `.key` 以及許多元件的大小都是以 `rem` 單位來定義的：它是指「相對於 __根元素__ 中指定的字形大小」。

### 動畫

動畫是利用 `transition` 來完成的。

```css
.key {
  /* 前略 */
  transition: all .07s ease;
  /*          屬性 時間 變化曲線 延遲（此略）*/
  /* 後略 */
}
```

更多 `transition`：http://www.w3schools.com/cssref/css3_pr_transition.asp

## JavaScript

看完影片後應該可以學到

-   利用 selector 尋找元素
    
    需要利用到`document.querySelector` 和 `document.querySelectorAll`。此外，也提及找尋特定屬性元素的作法：`.key[data-keycode="77"]`
-   按鍵事件的處理
    
    `window.keydown` 事件的應用
-   自訂屬性的應用
    
    `data-` 可以形成自訂屬性
-   播放音效
    
    `audio.play`
-   重頭播放音效
    
    配合 `audio.play` 以及 `audio.currentTime`
-   在 transition 停止後做對應的處理
    
    `element.transitionend` 事件的應用

更多 `audio`：https://developer.mozilla.org/en-US/docs/Web/API/HTMLAudioElement
