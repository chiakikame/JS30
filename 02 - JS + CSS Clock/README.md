# 筆記：第二日

## CSS

因為千秋的 CSS 很菜，所以順便學習一下

```css
.clock {
  /* 前略 */
  border-radius:50%; /* 形成圓形 */
  /* 中略 */
  box-shadow:
    0 0 0 4px rgba(0,0,0,0.1),  /* 外圈 */
    inset 0 0 0 3px #EFEFEF,    /* 內圈 */
    inset 0 0 10px black,       /* 內圈 */
    0 0 10px rgba(0,0,0,0.2);   /* 外圈 */
  /*
    多重的 box shadow。每個 shadow 之間以逗號隔開
    inset : 內陰影
    色彩前的數字分別代表 h-shadow v-shadow blur spread
    blur 和 spread 可以省略
   */
}
```

更多 `box-shadow` ：http://www.w3schools.com/cssref/css3_pr_box-shadow.asp

除此之外，本日重點在 `transform` 的使用。時針的角度以 `transform` 的 `rotate()` 來控制。旋轉中心需要另外調整，因為預設的旋轉中心在該物件的中心，跟時針的運作不一樣。

為了區別時針、分針、秒針，千秋對這些針的長度以及色彩做了調整。改色彩簡單，但是時針改長度之後，因為物件都是向左對齊，所以必須先向右移動到恰當的位置之後才可以做旋轉。

影片中順便說明了 `transition` 的變化曲線可以如何利用 Chrom dev tools 設定。附帶一提 Atom 編輯器有 `bezier-curve-editor` 可以使用。

## JS

相對於 CSS 來說，本日的 JS 就比較簡單了。畢竟必須先了解時鐘的結構才可以使用程式來操作它。

前述的「而由於時針調整過長度，所以必須先向右邊移動到恰當的位置後才可以旋轉。」這件事情也需要從程式上面來處理，因為在 `element.style.transform` 上面設定值的話會把 CSS 樣式表裡面的值蓋掉，也就是如果處理時針的時候忘記先移位後旋轉，時針就會處在很詭異的位置上。

了解時鐘的結構之後，剩下的就只是取得時間了。取得時間使用 `Date` 類別。

```JavaScript
let now = new Date();
```

如此一來 `now` 裡面就會包含該物件建立時的時間。了解當前時間之後，取得時、分、秒也非常直觀。剩下的就是計算角度：只要不要忘記加上 90 度就不會有問題。為啥要加上 90 度？從影片中可以看到加上 90 度後，針才會乖乖的落在時鐘 0 的位置上。

算好角度之後，修改 `element.style.transform` 把 `rotation` 放進去。對時針處理的話要先做 `translation`，像這樣:

```javascript
element.style.transform = 'translateX(100%) rotate(90deg)';
```
啊對了，影片最後提到，如果使用了 `transition`，那樣會在針要進入下一圈（也就是 59 和 0 的交界處）的位置做一個跑一圈回去的動畫，看起來挺礙眼的。所以，在這個交界處的時候，可以考慮暫停 `transition` 的效果，或用其他方法讓它看起來更自然些，或者乾脆不要用 `transition`！
