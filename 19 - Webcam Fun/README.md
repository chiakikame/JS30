# 筆記：第十九日

這次介紹：

* 取得網路攝影機的影像，並且放到 `video` 元素上面
* 把 `video` 上的影像擷取到 `canvas` 上面
* 把 `canvas` 上的資料匯出到圖檔上
* 對 `canvas` 上的資料做後處理

## 行前注意事項

因為取得網路攝影機資料的網頁必須是來自安全的來源（secured origin），所以這次我們必須準備個伺服器軟體：利用該軟體來傳送我們的網頁來進行測試。

若您電腦中有伺服器軟體的話，可以由該軟體來傳送網頁。若沒有的話，可以使用原作者準備好的系統來測試。若要使用原作者的系統，請先安裝好 `nodejs` 。

原作者所使用的系統會在您修改您的檔案以後，自動重新整理瀏覽器的畫面。對開發者來說是個很方便的功能。

## 取得影像

使用 `navigator.mediaDevices.getUserMedia` 以取得使用者的多媒體裝置。您可以傳送選項來指定要使用的裝置需要有哪些功能。 `getUserMedia` 會傳回 `Promise`。

取得了影像串流以後，為了讓 `video` 可以使用，則必須利用 `window.URL.createObjectURL` 取得該串流所對應的 URL，並且設定到 `video.src` 上面去。

設定完成後，要進行 `video.play` 來播放畫面；否則就只會有定住的畫格。

## 擷取影像到 `canvas` 上面

設定好 `canvas` 的寬度以及高度以後，使用 `context.drawImage(video, 0, 0)` 把在 `video` 中的影像繪製到 `canvas` 的原點上面。

## 把 `canvas` 上的資料匯出到圖檔上

使用 `canvas.toDataURL(mimeType)`。若 mimeType 是 image/jpeg 則會輸出 jpeg 圖片；若是 image/png 則會輸出 png 圖片。取得的 data url 將可以放在 `a.href` 上面供人下載，也可以放在 `img.src` 上供人瀏覽。

## 對 `canvas` 上的圖片做後處理

基本上，被畫在 `canvas` 上的圖片以點陣圖的形式放在記憶體中。點陣圖的資料可以利用 `context.getImageData(x, y, width, height)` 來取得。所取得的點陣圖資料以 RGBA 的順序來排列。

```
// imageData.data 的布局
      0         1
aidx  01234567890123456789
pidx  00001111222233334444
data  RGBARGBARGBARGBARGBA

* pidx: 像素的索引
* aidx: 陣列的索引
* data: RGBA 資料
```

排列的順序，由圖片的左上角開始，橫向依序排列

影像處理完成後，利用 `context.putImageData(data, x, y)` 把點陣圖資料放到 `canvas` 上

```js
const pixels = context.getImageData(0, 0, w, h);
pixels.data[0]; // 第 1 個像素的 R
pixels.data[1]; // 第 1 個像素的 G
pixels.data[2]; // 第 1 個像素的 B
pixels.data[3]; // 第 1 個像素的 A
... // 做些處理
context.putImageData(pixels, 0, 0);
```

## 其他小技巧

### `a.download` 屬性

若是設定 `a.download` 屬性，則可以決定使用者下載時使用的檔案名稱為何：

```html
<a href='...' download='hero'>Download me</a>
```

### `debugger` 陳述式

在 js 程式碼中放入 `debugger` 陳述式，就可以啟動除錯器，並且讓程式的執行暫停在這個位置。實例如下：

```js
console.log(largeData);
debugger;
```

若要在 node.js 底下使用這個陳述式，則要

```sh
node debug your_script.js
```

像這樣子來啟動你的程式。

更多 debug：

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/debugger
* https://nodejs.org/api/debugger.html
