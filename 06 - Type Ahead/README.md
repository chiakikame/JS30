# 筆記：第六日

這次主題是使用 `fetch API` 來取得資料、`promise` 的簡介、以及文字輸入提示的實做。

## 結構

`.search` 的輸入框接受輸入，然後利用 JS 在 `.suggestions` 的地方放上搜尋提示

## CSS

若想要對第奇數個物件以及對第偶數個物件作不同的處理，可以參考 `style.css` ：使用 `:nth-child(even)` 以及 `:nth-child(odd)` 就可以分別選取奇數以及偶數物件

## JavaScript

筆記分列如下

### fetch API

fetch 藉著 promise 回傳資料。原作者以 blob 命名回傳資料的本體，是因為 fetch API 不知道傳回來資料的本質為何。但以處理 JSON 資料來說，傳來的 blob 提供了 `json` 方法，所以不是問題。`blob.json()` 會得到一個 promise。

### Key events and input value

關於追蹤文字輸入框內資料的改變，若只使用 `change` 的話似乎有點不足：`change` 只會在使用者完成輸入之後才會發動（打字打到一半不會）；但是 `keyup`, `keydown`, `keypress` 也都各自有各自的遺漏點（可以參考[這裡](http://stackoverflow.com/questions/11365686/how-to-get-text-of-an-input-text-box-during-onkeypress)），因此原作者使用 `change` 和 `keydown` 雙管齊下。

### Promise

Promise 是一個不算小的主題。

以個人淺見，promise 可以改善在 async function 收結果時會產生的 callback hell:

```js
processAFile(file, (error, result) => {
  processContent(result, (error, parsedData) => {
    ...
  });
});
```

如果使用了 promise 的話，可以變成差不多像這樣的結構

```js
processAFile(file)
  .then((parsedData) => {
    return processContent(parsedData);
  })
  .then(...);
```

可以注意到洋蔥一般的分層變成了直線狀。

更多 promise：https://www.promisejs.org/

### Spread array into arguments

對於以下的定義

```JavaScript
const array = [1, 2, 3];
```

以下

```JavaScript
func(array[0], array[1], array[2]);
```

等同於

```JavaScript
func(...array);
```

### RegExp

想要從變數建立 `RegExp` 的話，必須使用它的建構式。如下：

```js
const pattern = new RegExp(inputBox.value, 'gi');
```

第二個參數中，`g` 表示 global， `i` 表示 case-insensitive （不分大小寫）
