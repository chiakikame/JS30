# 筆記：第五日

今天的內容大多以 CSS flexbox 為主

## CSS

### Flexbox

影片中有提到 nest flexbox container 的方法。

```css
.grandparent {
  display: flex;
}

.parent {
  flex: 1;
  display: flex;
}

.child {
  flex: 1;
}
```

如果想要更了解 flexbox 的話，可以看原作者（Wes Bos）的教學 [What the Flexbox](https://flexbox.io/)，或者是看[這裡](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)。

另外，有兩個有趣的互動式 flexbox 練習網站：

* http://flexboxfroggy.com/
* http://www.flexboxdefense.com/

### :first-child & :last-child

另外還有 `:first-child` 以及 `:last-child` 的使用。 `:first-child` 選擇「身為第一個子代者」，而 `:last-child` 選擇「身為最後一個子代者。

```html
<style>
  p:first-child {
    color:red;
  }
  p:last-child {
    color:green;
  }
</style>
<div>
  <p>我是紅的</p>
  <p>我用預設色</p>
  <p>我是綠的</p>
</div>
```

更多 `:first-child` ：http://www.w3schools.com/cssref/sel_firstchild.asp
（`:last-child` 的用法和 `:first-child` 非常相似）

### 其他

其他的技巧在之前的篇章中已經介紹過，故在此不贅述。

設置完成 CSS 後，接著寫 JavaScript

### 小技巧

使用添加 `border` 的方式可以比較容易觀察到欲觀察對象所佔用的大小。

## JavaScript

這裡展示了 `element.classList.toggle` 的使用。

另外，影片中提到，這個程式中，在針對 `flex` 屬性作 `transition` 的時候， Safari 瀏覽器中 `transitionend` 的 `event.propertyName` 會是 `flex`，而其他瀏覽器則是 `flex-grow`。
