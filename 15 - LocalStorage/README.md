# 筆記：第十五日

本日重點

* LocalStorage
* Event delegation

## JS

### `Form.submit` 事件

要防止 submit 發生時自動重新載入頁面的行為，使用 `e.preventDefault()`

### ES6 Object shorthand

```js
let item = 33;
let obj = {item: item};
```

等同於

```js
let item = 33;
let obj = {item};
```

### `localStorage`

您可以使用 `localStorage` 來存放 **字串**。注意它只能存放 **字串**。若放入的東西不是字串，就會被使用 `toString()` 轉成字串。

若想存放物件的話，必須先使用 `JSON.stringify` 把東西先轉換成可以轉得回來的字串再存放進去。

#### `localStorage` vs `cookies`

http://stackoverflow.com/questions/3220660/local-storage-vs-cookies

|      | `localStorage` | `cookies` |
| ---- | ---- | ---- |
| 主要資料流動 | 利用瀏覽器的 JS 調用 | 由瀏覽器傳送到伺服器 |
| 保存期限 | 不會過期 | 可能會過期 |
| 預設儲存空間 | 相對大 （約5 ~ 10 MB）* | 相對小 （約 4 KB） |
| 支援度 | 只有比較新的瀏覽器 | 都有 |

`localStorage` 的可用空間可因使用者的個人設定而有不同

關於儲存空間： http://stackoverflow.com/questions/2989284/what-is-the-max-size-of-localstorage-values

關於某個功能可否在某版本的瀏覽器上面使用，請參考 http://caniuse.com

### Event delegation （事件委託）

一般來說我們會把事件處理模式在一開始的時候先設定好。不過在其他的情況下，像是針對動態產生的元件，我們就沒有辦法預先設定事件處理器。雖說我們也可以動態元件生成的同時一併掛上事件處理器，然而這樣子費時費工，相當麻煩。

其實我們可以把事件處理器掛在動態元件的容器上面。如此一來該容器的子成員有事件產生的時候，卻沒有事件處理器時，該事件就會交給容器來處理。

這一層一層的事件處理模型跟一般的 GUI toolkit 設計上來說是大同小異的。若您接觸過其他的 GUI toolkit 的話應該不難理解這個概念。

### `Element.match`

這個 function 可以檢查一個元素是否符合某個 CSS selector

```js
item.match('li.students');
```

## HTML5

### `label.for`

```HTML
<li>
  <input type='checkbox' id='myItem'/>
  <label for='myItem'>My label</label>
</li>
```

如此一來，當使用者點選標籤本身時，該勾選方塊也會被勾選。（預設的情形中，使用者必須點勾選方塊，方塊才會有反應）

## CSS

### 自訂勾選方塊的顯示

作者首先把勾選方塊本身藏起來（`display: none;`），接著，利用狀態判斷 （`input:checked`） 在 label 前面加上元件（`label:before` 以及 `content`）

```CSS
/* 摘自 style.css */
.plates input + label:before {
  content: '⬜️';
  margin-right: 10px;
}

.plates input:checked + label:before {
  content: '🌮';
}
```
