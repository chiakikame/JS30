# 筆記：第二十三日

今天的主題是文字轉語音。

## 作業流程

* 使用 `speechSynthesis.getVoices()` 取得這個系統所提供的文字轉語音軟體名稱 （`name`） 以及語言清單。
* 創建一個新的 `SpeechSynthesisUtterance`，設定其 `voice`, `text`、`pitch` 以及 `rate` 屬性來調整語音的內容以及特性。 `voice` 屬性必須選自於 `speechSynthesis.getVoices()` 。
* `speechSynthesis.speak(utteranceInstance)` 來真正發出聲音

若要在電腦說話時令其停止，則使用 `speechSynthesis.cancel()`

## 與作者實作之差異

作者的實作中，在使用者調整使用者界面中的 Rate 以及 Pitch 時，語音會自動播放。千秋的設定是，使用者必須明確按下 Speak 按鈕，聲音才會播放。

## 問題

千秋的 Chromium 似乎沒有語音合成的功能，所以就無法測試了
