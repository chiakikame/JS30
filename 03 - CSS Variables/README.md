# 筆記：第三日

## CSS

本日的重頭戲在，執行時 CSS 變數！跟 SASS 等可以編譯到 CSS 的語言不同的在於，這些語言提供的是「編譯時變數」，也就是編譯完成後就不能任意修改的變數；而本篇提到的是「執行時變數」，是在執行的時候可以輕易用 JS 來修改的變數。

```html
<div class='level1'>
  <div class='level2'>
    <span>Hello world!</span>
  </div>
</div>
<div class="another-level1">
  <span>Hello Mars!</span>
</div>

<style>
.level1 {
  --my-variable: #00FF00; /* 宣告 */
}

.level2 {
  color: var(--my-variable); /* 使用 */
  /*
   該變數必須在使用者的親代（parent，無論直接或間接）中被宣告方可使用。
   尋找變數時，以離自己親緣關係比較近的優先使用。
   （其實跟 JS 的變數宣告以及尋找規則差不多，很直觀的）
   */
}

.another-level1 {
  color: #0000FF;
  /*
  在這裡沒辦法找到 --my-variable，因為 another-level1 的親代 （parent
  ，無論直接或者間接）都沒有 level1。反過來說，another-level1 並不屬於
  level1 的任何子代（children）成員。
   */
}
</style>
```

更多 CSS 變數：https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables

關於 CSS 變數的瀏覽器支援：http://caniuse.com/#feat=css-variables

## JavaScript

知道怎樣使用 CSS 變數了，那接著就要利用 CSS 變數來協助我們達成目標。幾個重點如下：

### input 的 change 以及 mousemove 事件

利用 input 的 change 以及 mousemove 事件，我們可以捕捉到數值的變動，並且把相對應的變動以 JS 傳送到 CSS 變數上。

### HTML 元素的 dataset 屬性

該屬性包含以 data-NNN 方式定義的各種屬性（像是 data-count, data-unit ...）。可以參考 https://developer.mozilla.org/en/docs/Web/API/HTMLElement/dataset

```html
<div id="data-source" data-name="Will"></div>

<script>
  console.log(document.querySelector('#data-source').dataset.name);
  // 會在開發者工具中列印出 'Will' 字樣
</script>
```

### HTML 根元素的取得

取得根元素可以使用 `document.documentElement`。以 `HTML` 文件來說總會是 `html` 標籤。即使該網頁沒有定義 `html` 標籤也一樣：

```html
<!-- 另存成 html 文件（例如 1.html），然後用瀏覽器開啟 -->
<div class='root'>
  <div><span>hello</span></div>
  <script>
    console.log(document.documentElement); // 列出 <html>
  </script>
</div>
```
