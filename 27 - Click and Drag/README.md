# 筆記：第二十七日

本日的目標是完成一個可以用滑鼠游標拖曳捲動的功能。這個功能可以藉著組合之前講過的 `mouse*` 事件來完成。

## 做法概說

想辦法利用滑鼠事件取得游標移動的變化量，接著把變化量加入捲動量變數 （`element.scrollLeft`）即可。

## Firefox 和 Chromium 的差異

即使設定了 `event.preventDefault()`，在 Firefox 中，拖曳時文字還是會被選起來。 Chromium 則不會有這樣的現象。
