# 筆記：第四日

本日的影片內容只有 JS。

片子裡面的內容，如果您曾經碰過 haskell, kotlin 或者甚至 python 的話可能不太陌生，所以影片作者建議諸位可以自行先嘗試一下再回來看片。當然若您是超級新手的話就先看片子吧。

## JavaScript

本日的重點 / 小秘訣如下

### filter / map / reduce

`filter` 用在保留需要的元素；`map` 用在處理整串元素（所以輸出與輸入 Array 長度相同）；`reduce` （某些程式語言中被稱作 `fold`）則協助您利用 array 的元素建立一個最後的結果（像是總和、平均等）。`filter`, `map`, `reduce` 都不會改變原本 array 的內容，而是產生一個新的 array 或是資料來傳回。

樣板如下：

```js
// filter 的樣板
arr.filter(function (arrayElement, indexOfElement, arrayItself) {
  // 您需要手動編寫這個部份（在這裡放入您的處理程式碼）
  // 利用傳回值來告訴 filter 你想不想保留這個元素：
  // 傳回（return） true 或者可以轉換為 true 的資料，則表示你想保留這個元素。
  // 其他的狀況均表示你不要資料
});

// map 樣板
arr.map(function (arrayElement, indexOfElement, arrayItself) {
  // 您需要手動編寫這個部份（在這裡放入您的處理程式碼）
  // 在這裡您把資料處理好之後傳回（return）
});

// reduce 樣板
arr.reduce(function (accumulatedData, arrayElement,
  indexOfElement, arrayItself) {
  // 您需要手動編寫這個部份（在這裡放入您的處理程式碼）
  // 資料的處理由 array 的第一個元素開始處理
  //（也就是輸出成字串後最左邊或是最上面的元素）
  // 這裡的程式碼（由您負責撰寫）把 arrayElement 的資料（經過計算）合併到 `accumulatedData` 中
  // 合併完成後，傳回（return）合併完成的結果
  //
  // 處理第一個元素時， accumulatedData === initialValue
}, initialValue);
```

另外，`map` 和 `filter` 都可以使用 `reduce` 實做。`reduce` 等於說是比較一般性（general case）的工具。（可以參考 http://stackoverflow.com/questions/5726445/how-would-you-define-map-and-filter-using-foldr-in-haskell ）

需要注意這些好工具只能在 Array 類別衍生的物件上使用。

### 排序（sort）的規則

`sort` 是 **in-place** 排序，也就是本來的 array 會被修改。如果在意的話，先產生原本 array 的副本，然後再排序之。`sort` 會傳回排序好的 array。

```js
arr.sort(function compare(a, b) {
  // `sort` 取出兩個東西，詢問您此二物件的順序
  // 利用不同的傳回（return）值，您告訴 sort 這兩個物件的順序
  //
  // return -N : a 會排到比 b 前面的地方
  // return 0 : 順序未定義
  // return N : b 會排到比 a 前面的地方
  // (N: 表示任意正數)
});
```

更多 `sort` ： https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
 
### 從 NodeList 建立 Array 的方法

```js
let nodeList = document.querySelectorAll('.data');
let anArray = Array.from(nodeList);
let anotherArray = [...nodeList]; // ES6 style
```

### 在開發者工具以表格顯示資料

```js
console.table(object);
```

這樣可以在 console 的地方，利用表格的樣式來輸出資料，較為賞心悅目。

### 取代 if-then-else 的三元運算子（ternary operator)

```js
let val = condition ? a : b;
```

等同於

```js
let val;
if (condition) {
  val = a;
} else {
  val = b;
}
```
