# 筆記：第十一日

今天可以學到一些操作 `video` ，影片播放標籤，的方法。

而今天的實做目標就是把 `video` 的狀態和一個既有的使用者界面連結在一起。

## 重點條列

### 動態地決定要使用的元素 / 要呼叫的方法

因為 `obj.key` 等同於 `obj['key']`，所以 `obj.key()` 等同於 `obj['key']()`。所以說：

```js
if (video.paused) {
  video.play();
} else {
  video.pause();
}
```

等同

```js
const func = video.paused ? 'play' : 'pause';
video['func']();
```

Groovy, Python 似乎也有類似的特性。果然動態語言還是有其優點。

### 進度條的顯示

作者把進度條設計成，只要修改 `div.progress__filled` 中 `flex-basis` 的百分比即可，因此若要反映播放進度的話就從這裡下手。

### 全螢幕

可以參考下列連結：

* https://developer.mozilla.org/samples/domref/fullscreen.html
* https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API

比較麻煩的地方是要應付各個瀏覽器之間的實做相異點。

### 關於事件處理器 / 元素上的 offsetX, offsetWidth 以及同伴們

請參考這組資料

* http://stackoverflow.com/questions/21064101/understanding-offsetwidth-clientwidth-scrollwidth-and-height-respectively
* http://jsfiddle.net/y8Y32/25/


### 其他

因為使用者可能會使用其他方法來更動影片播放的狀態（像是使用 plugin），所以說，依靠 `togglePlay` （被掛在 `video.click` 以及 `toggle.click` 上的事件處理程式）來修改播放按鈕的狀態不妥當。利用影片本身的狀態反應事件觸發與否來處理按鈕的狀態則較為恰當。
