// 元素

const playerDiv = document.querySelector('div.player');
const video = document.querySelector('video');
const playButton = document.querySelector('button.toggle');
const fullScreenButton = document.querySelector('button.fullScreen');
const progressBar = document.querySelector('div.progress');
const progressBarFill = document.querySelector('div.progress__filled');
const playerSlideBars = document.querySelectorAll('input.player__slider');
const jumpButtons = document.querySelectorAll('button[data-skip]');

// 重設播放器狀態

progressBarFill.style.flexBasis = "0%";
playerSlideBars.forEach(e => e.value = 1);

// 全螢幕功能測試

const prefix = document.mozCancelFullScreen ? 'moz' : 'webkit';
const fullScreenTester = prefix === 'moz' ? 'mozFullScreen' : 'webkitIsFullScreen';

// 功能
//
// 播放
playButton.addEventListener('click', togglePlay);
video.addEventListener('click', togglePlay);

// 音量 & 播放速度

playerSlideBars.forEach(bar => {
  bar.addEventListener('change', reflectSliderValue);
  let mouseDown = false;
  bar.addEventListener('mousedown', () => mouseDown = true);
  bar.addEventListener('mouseup', () => mouseDown = false);
  bar.addEventListener('mousemove', e => {
    if (mouseDown) reflectSliderValue(e);
  });
});

// 使用進度條
progressBar.addEventListener('click', adjustVideoProgress);

// 向前向後跳
jumpButtons.forEach(jumpButton => {
  jumpButton.addEventListener('click', e => {
    video.currentTime += parseFloat(e.target.dataset.skip);
  });
});

// 全螢幕
// 測試於 Firefox 50 & Chromium 55
fullScreenButton.addEventListener('click', () => {
    if (document[fullScreenTester]) {
      document[`${prefix}CancelFullScreen`]();
    } else {
      playerDiv[`${prefix}RequestFullScreen`]();
    }
});

// 播放器反饋
video.addEventListener('play', () => {
  playButton.textContent = "| |";
});

video.addEventListener('pause', () => {
  playButton.textContent = "►";
});

video.addEventListener('timeupdate', () => {
  const percentage = (video.currentTime / video.duration) * 100;
  progressBarFill.style.flexBasis = `${percentage}%`;
});

//
// 功能區塊結束
 
// 輔助程式碼

function togglePlay() {
  video[video.paused ? 'play' : 'pause']();
}

function reflectSliderValue(e) {
  const bar = e.target;
  video[bar.name] = bar.value;
}

function adjustVideoProgress(e) {
  video.currentTime = (e.offsetX / progressBar.offsetWidth) * video.duration;
}
