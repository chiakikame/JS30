# 筆記：第十六日

這次的內容是，製作會跟著滑鼠位置做出反應的陰影。影片中也提到要如何製作多重陰影。

## `contenteditable`

可以設定在 HTML 標籤上來讓使用者可以編輯某個元素

## CSS `textShadow`

一個文字可以有很多個陰影。每個陰影描述之間以逗號區隔開

```CSS
h1 {
  text-shadow: 12px 13px 0 hsl(0, 80%, 55%),
    -12px -13px 0 hsl(90, 80%, 55%),
    13px -12px 0 hsl(180, 80%, 55%),
    -13px 12px 0 hsl(270, 80%, 55%);
}
```

## 如何知道當前游標位置對應於上層容器的位置為何

在本例子中，已經知道 `h1` 只有一個上層容器 `div.hero`，所以直接

```javascript
x = e.offsetX + e.target.offsetLeft;
```

若有很多層，則必須使用 `offsetParent` 以及 `offsetLeft` 進行遞迴追蹤。

您也可以參考如下的連結：
http://stackoverflow.com/questions/11634770/get-position-offset-of-element-relative-to-a-parent-container
