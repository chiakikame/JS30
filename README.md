## 緣起

看到 Quincy Larson [在 Medium 上](https://medium.com/@quincylarson/free-code-camp-has-dozens-of-projects-you-can-just-dive-in-and-build-b9ccfe2d7051#.xrplhbcsh)推薦了 JavaScript 30，所以就嘗試看看。

JavaScript 30 的官方網站：https://javascript30.com/

## 請一定要親自看影片！

即使您在不看影片的狀態下自己想辦法刻出了作者所給定的目標，若是不看影片的話就學不到作者在影片中穿插的珍貴小技巧了。在加上作者的說明技巧很有層次以及邏輯，很推薦一定要觀看影片！

您一定是想要學點東西，才會想要看作者提供的影片吧！如果您自認程度已經超過作者...

## 程式碼排列

原則上跟 JavaScript 30 上的排列相同，每天的練習內容有各自的資料夾。在動手撰寫前，先把練習的檔案建立起來，做一個 commit 並且 tag 之（像是 d01-start），接著，在完工後的 commit 上面做相對應的 tag（例如 d01-end-nn，nn 代表第幾次的修改）。

以第一天的內容為例子，您可以使用

```sh
git diff d01-start d01-end-001 "01 - JavaScript Drum Kit/index.html"
```

來檢視完成前後的變化。

## 完成狀態

| Day  | Start Tag | End Tag     |
| ---- | --------- | ----------- |
| 01   | d01-start | d01-end-001 |
| 02   | d02-start | d02-end-001 |
| 03   | d03-start | d03-end-001 |
| 04   | d04-start | d04-end-001 |
| 05   | d05-start | d05-end-001 |
| 06   | d06-start | d06-end-001 |
| 07   | d07-start | d07-end-001 |
| 08   | d08-start | d08-end-001 |
| 09   | d09-start | d09-end-001 |
| 10   | d10-start | d10-end-001 |
| 11   | d11-start | d11-end-001 |
| 12   | d12-start | d12-end-001 |
| 13   | d13-start | d13-end-001 |
| 14   | d14-start | d14-end-001 |
| 15   | d15-start | d15-end-001 |
| 16   | d16-start | d16-end-001 |
| 17   | d17-start | d17-end-001 |
| 18   | d18-start | d18-end-001 |
| 19   | d19-start | (note only) |
| 20   | d20-start | (note only) |
| 21   | d21-start | (note only) |
| 22   | d22-start | d22-end-001 |
| 23   | d23-start | d23-end-001 |
| 24   | d24-start | d24-end-001 |
| 25   | d25-start | d25-end-001 |
| 26   | d26-start | d26-end-001 |
| 27   | d27-start | d27-end-001 |
| 28   | d28-start | d28-end-001 |
| 29   | d29-start | d29-end-001 |
| 30   | d30-start | d30-end-001 |
