# 筆記：第二十日

主題是語音辨識

講到語音辨識的話，最重要的類別就是 `SpeechRecognition` （chrome 的話是 `webkitSpeechRecognition`）。建構好語音辨識物件之後，監聽 result 事件就可接收到語音辨識的結果。

注意到，若使用 chrome 的話，會因為安全性原則的關係而無法藉著直接載入網頁進行測試。您必須通過一個伺服器軟體來傳送您的網頁才行。這一點跟上一個練習一樣。另外， firefox 桌面板無法使用這項功能。

回到 result 事件。該事件處理器會收到一個 `SpeechRecognitionEvent`。您可以自其中的 `results` 欄位中，取得 `SpeechRecognitionResultList`。`SpeechRecognitionResultList` 由 `SpeechRecognitionResult` 構成，而 `SpeechRecognitionResult` 內部又由 `SpeechRecognitionAlternative` 構成，簡直就是洋蔥。

```js
sr.addEventListener('result', e => {
  const results = e.results;
  const firstTranscript = e.results[0][0].transcript;
  const confidenceOfFirstTranscript = e.results[0][0].confidence;
  
  const transcriptList = Array.from(e.results)
    .map(result => result[0].transcript);
});
```

好，那可以辨識語音了（語音轉成文字），那我們要如何掌握使用者說完話的時間點呢？您可以監聽 `end` 事件（雖說這事件其實代表語音辨識服務已經暫時下線，需要使用 `start` 再啟動才能繼續辨識），或者檢查 `SpeechRecognitionResult.isFinal` 屬性。您利用這兩種方式，多半可以猜想使用者的輸入已經告一個小段落了。
