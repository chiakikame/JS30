# 筆記：第二十六日

今天要真的把神奇的動畫下拉式選單做出來了！

## 架構

下拉式選單放內容用的白框框在 `index.html` 第 10 行左右的地方：

```html
<div class="dropdownBackground">
  <span class="arrow"></span>
</div>
```

接著的清單內的 `li` 則放了觸發輔助 `a`，以及下拉式選單真正的內容 `div`。我們會把觸發事件處理器放在 `li` 上面，因為這樣就可以讓使用者在把滑鼠移動到下拉式選單內容時，選單不會跑掉。

```html
<ul class="cool">
  <li>
    <a href="#">About Me</a>
    <div class="dropdown dropdown1">
      <div class="bio">
        <!--略-->
      </div>
    </div>
  </li>
  <!--中略-->
</ul>
```

所以接著就要做出事件處理來偵測 `mouseenter` 以及 `mouseleave` 事件，並且在這兩個事件發生時恰當的設定版面，來達到顯示下拉式選單的效果。

作者的設計中， `.dropdown` 的隱藏是利用 `display: none;` 以及 `opacity: 0;` 完成的，然而若想做出動態變化的效果（利用 `transition`）的話，`display` 和 `opacity` 的變化不能同時進行，否則會無法進行動態變化。為了解決這個問題，作者把這兩個變化拆開：在要顯示選單時，把 `display` 先設定為 `block`，接著再來調整 `opacity`。這樣一來才會有平順的轉場效果。

好，那可以顯示下拉式選單的內容後，接著下來要用 `.dropdownBackground` 幫下拉式選單加上底。要加上底，就要把 `.dropdownBackground` 顯示出來、移動到正確的位置上，並且調整其大小，來覆蓋整個下拉式選單的內容。

## JS this

在撰寫 JS 的時候，使用到 `this` 時必須仔細思考 `this` 在此處所代表的意義。

## 實作問題

在 firefox 下面，跳出的內容會向右邊偏，而 chromium 不會。目前還在研究該問題的成因當中。根據 [這裡](https://googlechrome.github.io/samples/css-flexbox-abspos/index.html)，這代表 firefox 沒有完全按照 flexbox 的標準來實作 flexbox。
