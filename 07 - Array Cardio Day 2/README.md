# 筆記：第七日

本週的重點放在 array 的處理上面：介紹了 `some`, `every`, `find`, `findIndex` 四個工具。這四個工具的共通點在，和之前介紹的 `filter` 一樣，傳入一個條件判斷函數，然後就可以幫你檢視某個陣列是否符合特殊條件：

* `some` 檢視陣列中是否有任意成員符合條件
* `every` 檢視陣列中的所有成員是否均符合條件
* `find` 找出陣列中第一個符合條件的成員
* `findIndex` 找出陣列中第一個符合條件成員的索引

附帶的小重點是，如何刪掉某個特定的成員。要達成這樣的目標，至少有三種作法：

* 用 `filter` 保留其他要的部份
* 用 `splice` 修改本來的 array，把不要的直接拿掉
* 用 `slice` 取得前後的部份然後再組合起來

## 小技巧

使用

```js
const myVariable = 123;
console.log(myVariable);
```

的話，在 console 只會列出 `myVariable` 的內容：

```
123
```

但若是這樣子傳入 `console.log`：

```js
const myVariable = 123;
console.log({myVariable});
```

則會讓 `myVariable` 被包裹在 `Object` 中，一併列出變數的名稱：

```
Object { myVariable : 123 }
```
